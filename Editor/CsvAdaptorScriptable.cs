using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace GDC.Innovation.Scriptable.Editor
{
    using static ScriptableConvert;
    public class CsvAdaptorScriptable<T, U> : ScriptableAdaptor<T> where T : ScriptableObject where U : new()
    {
        public string rootPathCsv;
        public string rootScriptable;

        public override void LoadScriptable()
        {
            var pathJson = GetFullPath(rootPathCsv, false);
            string[] files = Directory.GetFiles(pathJson, "*.csv");
            foreach (var file in files)
            {
                var data = IntermediateConvert(LoadFormCSV<U>(file, false));
                SaveToScriptable(data, rootScriptable, Path.GetFileNameWithoutExtension(file));
            }
        }

        public virtual T IntermediateConvert(IEnumerable<U> data)
        {
            return data as T;
        }

        public virtual IEnumerable<U> IntermediateRevert(T data)
        {
            return data as IEnumerable<U>;
        }

        public override void SaveScriptable()
        {
            string[] assets = Directory.GetFiles(rootScriptable, "*.asset");
            foreach (var asset in assets)
            {
                var so = AssetDatabase.LoadAssetAtPath<T>(asset);
                SaveToCSV(IntermediateRevert(so), Path.Combine(rootPathCsv, so.name + ".csv"), false);
            }
            AssetDatabase.Refresh();
        }
    }
}
