using Gtion.Plugin.CustomInspector.Button;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace GDC.Innovation.Scriptable.Editor
{
    public class ScriptableAdaptor<T> : ScriptableObject where T : ScriptableObject
    {
        public string CurrentDirectory { get { return Directory.GetCurrentDirectory() + '/'; } }

        [Button]
        public virtual void LoadScriptable(){ }

        [Button]
        public virtual void SaveScriptable(){ }

        public void SaveToScriptable(T data, string scriptablePath, string scriptableName)
        {
            scriptablePath = Path.Combine(scriptablePath, scriptableName + ".asset");
            AssetDatabase.DeleteAsset(scriptablePath + scriptablePath);
            AssetDatabase.CreateAsset(data as T, scriptablePath);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
    }
}
