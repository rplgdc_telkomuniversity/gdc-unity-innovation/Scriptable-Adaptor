using System.IO;
using UnityEditor;
using UnityEngine;

namespace GDC.Innovation.Scriptable.Editor
{
    using static ScriptableConvert;
    public class JsonAdaptorScriptable<T> : ScriptableAdaptor<T> where T : ScriptableObject
    {
        public string rootPathJson;
        public string rootScriptable;

        public override void LoadScriptable()
        {
            var pathJson = GetFullPath(rootPathJson, false);
            string[] files = Directory.GetFiles(pathJson, "*.json");
            foreach (var file in files)
            {
                var data = LoadFromJSON<T>(file, false);
                SaveToScriptable(data, rootScriptable, Path.GetFileNameWithoutExtension(file));
            }
        }

        public override void SaveScriptable()
        {
            string[] assets = Directory.GetFiles(rootScriptable, "*.asset");
            foreach (var asset in assets)
            {
                var so = AssetDatabase.LoadAssetAtPath<T>(asset);
                SaveToJSON(so, Path.Combine(rootPathJson, so.name + ".json"), false);
            }
            AssetDatabase.Refresh();
        }
    }
}
