# Scriptable Adaptor
## Built In
![Build Status](https://badgen.net/badge/Build/Passing/green) ![Version](https://badgen.net/badge/Release/v1.0.0/blue) ![Languages](https://badgen.net/badge/Languages/C-Sharp/blue)
![Engine](https://badgen.net/badge/Engine/Unity%202020.3.4f1/gray)

Depedencies
- "com.gtion.custominspector": "https://gitlab.com/rplgdc_telkomuniversity/gdc-unity-innovation/gtion-custom-inspector.git"

## Installation
Please check documentation below
> https://docs.unity3d.com/2019.3/Documentation/Manual/upm-ui-giturl.html

## What is it?
Scriptable Adaptor for JSON & CSV File.

## Feature List
- JSON to Scriptable.
- Scriptable to JSON.
- CSV to Scriptable.
- Scriptable to CSV.

## Getting Started
### JSON Adaptor Setup
![image info](./image/json-1.png)

let's say, your json file like that.

1. Create Scriptable Object script
    ```cs
    using UnityEngine;
    
    [CreateAssetMenu(menuName ="PeopleData")]
    public class PeopleData : ScriptableObject
    {
        public string firstName;
        public string lastName;
        public int age;
    }
    ```
2. Create JSON Adaptor Editor Script to load json.
    ```cs
    using GDC.Innovation.Scriptable.Editor;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Adapter/People")]
    public class PeopleAdapter : JsonAdaptorScriptable<PeopleData>
    {

    }
    ```
3. Create instance JSON Adaptor

    ![image info](./image/json-2.png)
4. fill path to generate scriptable

    ![image info](./image/json-3.png)
    
*note : make sure, only one type of scriptable object in scriptable path.

### CSV Adaptor Setup
![image info](./image/csv-1.png)

let's say, your csv file like that.

1. Create Scriptable Object script & intermediate class
    ```cs
    using System.Collections.Generic;
    using UnityEngine;
    using System.Linq;

    [CreateAssetMenu(menuName = "PersonData")]
    public class PersonData : ScriptableObject
    {
        public string[] firstName;
        public string[] lastName;
        public int[] age;

        public void LoadIntermediate(IEnumerable<PersonDataIntermediate> intermediate)
        {
            firstName = intermediate.Select(i => i.firstName).ToArray();
            lastName = intermediate.Select(i => i.lastName).ToArray();
            age = intermediate.Select(i => i.age).ToArray();
        }
    }

    public class PersonDataIntermediate
    {
        public string firstName;
        public string lastName;
        public int age;
    }
    ```
2. Create CSV Adaptor Editor Script to load csv.
    ```cs
    using GDC.Innovation.Scriptable.Editor;
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(menuName = "Adapter/Person")]
    public class PersonAdapter : CsvAdaptorScriptable<PersonData, PersonDataIntermediate>
    {
        public override PersonData IntermediateConvert(IEnumerable<PersonDataIntermediate> data)
        {
            var scriptable = CreateInstance<PersonData>();
            scriptable.LoadIntermediate(data);
            return scriptable;
        }

        public override IEnumerable<PersonDataIntermediate> IntermediateRevert(PersonData data)
        {
            var intermediates = new List<PersonDataIntermediate>();
            int count = data.firstName.Length;
            for (int i = 0; i < count; i++)
            {
                intermediates.Add(new PersonDataIntermediate());
                intermediates[i].firstName = data.firstName[i];
                intermediates[i].lastName = data.lastName[i];
                intermediates[i].age = data.age[i];
            }
            return intermediates;
        }
    }
    ```
3. Create instance JSON Adaptor & fill path to generate scriptable

    ![image info](./image/csv-2.png)

*note : make sure, only one type of scriptable object in scriptable path.

## Contributor


| Profile | 
| ------ |
| [![Anas](https://avatars.githubusercontent.com/u/47500014?s=60&v=4)](https://www.linkedin.com/in/anasrasyid/) |
| [Anas Rasyid](https://www.linkedin.com/in/anasrasyid/) | 

## License

MIT
