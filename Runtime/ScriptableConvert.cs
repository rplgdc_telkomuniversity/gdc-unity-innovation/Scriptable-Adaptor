using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace GDC.Innovation.Scriptable
{
    public static class ScriptableConvert
    {
        public static string GetFullPath(string path, bool isPersistence = true)
        {
            var root = (isPersistence) ? Application.persistentDataPath : Directory.GetCurrentDirectory();
            return Path.Combine(root, path);
        }

        public static void SaveToJSON<T>(T data, string path, bool isPersistence = true)
        {
            path = GetFullPath(path, isPersistence);
            var value = JsonConvert.SerializeObject(data, Formatting.Indented);
            File.WriteAllText(path, value);
        }

        public static T LoadFromJSON<T>(string path, bool isPersistence = true)
        {
            path = GetFullPath(path, isPersistence);
            if (!File.Exists(path))
            {
                Debug.LogWarning($"File {path} is not exist.");
            }
            string json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static IEnumerable<T> LoadFormCSV<T>(string path, bool isPersistence = true) where T : new()
        {
            path = GetFullPath(path, isPersistence);
            return CsvConvert.LoadObjects<T>(path);
        }

        public static void SaveToCSV<T>(IEnumerable<T> data, string path, bool isPersistence = true)
        {
            path = GetFullPath(path, isPersistence);
            CsvConvert.SaveObjects(data, path);
        }
    }
}
