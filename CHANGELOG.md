# Changelog
## [1.0.1] - 2021-08-23
### Fix
- Changelog meta file

## [1.0.0] - 2021-08-20
### Added
- Getting Started
- JSON Scriptable 
- CSV Scriptable (EXPERIMENTAL)
- Initial Realese